package nuki

import (
	"context"
	"fmt"
	"net/http"

	"github.com/mattermost/mattermost/server/public/pluginapi"
)

type Client struct {
	log         pluginapi.LogService
	url         string
	smartLockID string
	bearer      string
	client      *http.Client
	sendReq     bool
}

func New(client pluginapi.LogService, url, bearer, smartlockID string) Client {
	return Client{
		log:         client,
		url:         url,
		bearer:      bearer,
		smartLockID: smartlockID,
		client:      http.DefaultClient,
		sendReq:     true,
	}
}

func (n *Client) Lock(ctx context.Context) error {
	url := fmt.Sprintf("%s/smartlock/%s/action/lock", n.url, n.smartLockID)
	if err := n.do(ctx, url); err != nil {
		return err
	}

	return nil
}

func (n *Client) Unlock(ctx context.Context) error {
	url := fmt.Sprintf("%s/smartlock/%s/action/unlock", n.url, n.smartLockID)
	if err := n.do(ctx, url); err != nil {
		return err
	}

	return nil
}

func (n *Client) do(ctx context.Context, url string) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, nil)
	if err != nil {
		return fmt.Errorf("new request failed: %w", err)
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer: %s", n.bearer))

	if n.sendReq {
		resp, err := n.client.Do(req)
		if err != nil {
			return fmt.Errorf("sending request failed: %w", err)
		}

		if resp.StatusCode != http.StatusNoContent {
			return fmt.Errorf("request failed: %s", resp.Status)
		}

	}

	return nil
}
