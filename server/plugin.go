package main

// See https://developers.mattermost.com/extend/plugins/server/reference/

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/digilab.overheid.nl/ecosystem/mattermost-door-lock/server/nuki"

	"github.com/mattermost/mattermost/server/public/plugin"
	"github.com/mattermost/mattermost/server/public/pluginapi"
)

// Plugin implements the interface expected by the Mattermost server to communicate between the server and plugin processes.
type Plugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration

	client *pluginapi.Client

	// doorLockChannel
	doorLockChannel string

	nukiClient nuki.Client

	// BotUserID
	botUserID string

	// serverUrl
	serverURL string
	// pluginPath
	pluginPath string

	// signingKey
	signingKey []byte

	router *chi.Mux
}

var Router = chi.NewRouter()

func (p *Plugin) Print(v ...interface{}) {
	p.client.Log.Warn(fmt.Sprint(v...))
}

func (p *Plugin) InitAPI() *chi.Mux {
	pluginRouter := chi.NewRouter()

	pluginRouter.Use(middleware.Recoverer)
	pluginRouter.Use(middleware.RequestID)
	pluginRouter.Use(middleware.RequestLogger(&middleware.DefaultLogFormatter{
		Logger:  p,
		NoColor: true,
	}))
	pluginRouter.NotFound(p.handleNotFound)

	pluginRouter.Group(func(r chi.Router) {
		// require the configuration to be set for routes handling activity for the door lock
		r.Group(func(r chi.Router) {
			r.Post("/integration/response", p.integrationResponse)
		})
	})

	return pluginRouter
}

func (p *Plugin) ServeHTTP(_ *plugin.Context, w http.ResponseWriter, r *http.Request) {
	p.client.Log.Debug("New request:", "Host", r.Host, "RequestURI", r.RequestURI, "Method", r.Method, "path", r.URL.Path)

	p.router.ServeHTTP(w, r)
}
