package main

import (
	"github.com/google/uuid"
	"github.com/mattermost/mattermost/server/public/plugin"
)

type Activity struct {
	ID     uuid.UUID `json:"id"`
	Owner  string    `json:"owner"`
	Action string    `json:"action"`
}

func main() {
	doorLockPlugin := &Plugin{}

	plugin.ClientMain(doorLockPlugin)
}

func (p *Plugin) OnActivate() error {
	p.router = p.InitAPI()

	// roomConfig, err := LoadEmbeddedRoomConfig()
	// if err != nil {
	// 	return err
	// }

	// p.roomConfig = roomConfig
	return nil
}
