package main

import (
	"errors"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/plugin"
)

// When a message is received, check if it is a command for the door lock bot
func (p *Plugin) MessageHasBeenPosted(_ *plugin.Context, post *model.Post) {
	// Ignore messages sent by the bot itself
	if sentByPlugin, _ := post.GetProp(DoorLockBotMessageMarker).(bool); sentByPlugin {
		return
	}
	// Ignore system messages announcing the bot leaving the channel
	if post.Type == model.PostTypeRemoveFromChannel {
		return
	}

	// Get the channel the message was posted in
	channel, err := p.client.Channel.Get(post.ChannelId)
	if err != nil {
		p.client.Log.Error("error getting channel", "error", err)
		return
	}

	// Check if it is a direct message
	_, err = p.handleDirectMessage(channel, post)
	if err != nil {
		p.client.Log.Error("error handling direct message", "error", err)
		return
	}

	// Check if it is a message announcing someone joining our private channel

	_, err = p.handleJoinedDoorlockChannel(channel, post)
	if err != nil {
		p.client.Log.Error("error handling door lock channel join", "error", err)
		return
	}

	// Not interested in this message
}

func (p *Plugin) handleJoinedDoorlockChannel(channel *model.Channel, post *model.Post) (bool, error) {
	// Check if we joined or have been added to a channel
	if post.Type != model.PostTypeAddToChannel && post.Type != model.PostTypeJoinChannel {
		return false, nil
	}

	// Check if we joined the door lock channel
	if channel.Id != p.doorLockChannel {
		return false, nil
	}

	addedUserId, ok := post.GetProp("addedUserId").(string)
	if !ok {
		return false, errors.New("could not retrieve the added user id")
	}

	channel, err := p.API.GetDirectChannel(p.botUserID, addedUserId)
	if err != nil {
		return false, err
	}

	// Send message containing the buttons
	if err := p.sendActivityMessage(channel.Id); err != nil {
		return true, err
	}

	return true, nil
}

// Returns (handled, error):
// - handled is true if the message was handled by this function
// - error if an error occurred
func (p *Plugin) handleDirectMessage(channel *model.Channel, post *model.Post) (bool, error) {
	// Check if it is a private message
	if channel.Type != model.ChannelTypeDirect {
		// Not a direct message
		return false, nil
	}

	// Check if bot is member of the channel
	botMembers, appErr := p.client.Channel.ListMembersByIDs(post.ChannelId, []string{p.botUserID})
	if appErr != nil {
		p.client.Log.Error("error getting channel members", "error", appErr)
		return false, appErr
	}
	if len(botMembers) == 0 {
		// Bot is not a member of the channel
		return false, nil
	}

	// Check if user is member of p.doorLockChannel
	members, err := p.client.Channel.ListMembersByIDs(p.doorLockChannel, []string{post.UserId})
	if err != nil {
		p.client.Log.Error("error getting channel member", "error", err)
		return false, err
	}
	if len(members) == 0 {
		// User is not a member of the channel
		err = p.sendMembershipMessage(post.ChannelId)
		if err != nil {
			p.client.Log.Error("error sending membership message", "error", err)
			return false, err
		}
		return false, nil
	}

	if err := p.sendActivityMessage(post.ChannelId); err != nil {
		return true, err
	}

	return true, nil
}
