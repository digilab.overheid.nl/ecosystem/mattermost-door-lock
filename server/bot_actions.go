package main

import (
	"fmt"

	"github.com/mattermost/mattermost/server/public/model"
)

func (p *Plugin) notifyNewActivity(activity *Activity) error {
	user, err := p.client.User.Get(activity.Owner)
	if err != nil {
		return err
	}

	action := ""
	switch activity.Action {
	case "open":
		action = "opened"
	case "lock":
		action = "locked"
	}

	message := fmt.Sprintf("@%s : `%s` the door",
		user.Username,
		action,
	)

	// Notify the channel of a new activity
	err = p.sendBotMessage(p.doorLockChannel, message, model.StringInterface{})
	if err != nil {
		return err
	}

	return nil
}

func (p *Plugin) sendMembershipMessage(channelID string) error {
	message := "Je moet lid zijn van het deurslot kanaal om hem open te kunnen doen."

	_, err := p.sendUserMessage(p.botUserID, channelID, message, model.StringInterface{})
	return err
}

func (p *Plugin) sendActivityMessage(channelID string) error {
	message := fmt.Sprintf(":wave:")

	props := model.StringInterface{
		"attachments": []*model.SlackAttachment{
			{
				Actions: []*model.PostAction{
					{
						Id:    "open",
						Name:  "Open",
						Style: "success",
						Integration: &model.PostActionIntegration{
							URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-door-lock"), // IMPROVE: get plugin slug from manifest
							Context: map[string]any{
								"response": "open",
							},
						},
					},
					{
						Id:    "lock", // Note: the action ID must not contain underscores
						Name:  "Lock",
						Style: "danger",
						Integration: &model.PostActionIntegration{
							URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-door-lock"), // IMPROVE: get plugin slug from manifest
							Context: map[string]any{
								"response": "lock",
							},
						},
					},
				},
			},
		},
	}

	_, err := p.sendUserMessage(p.botUserID, channelID, message, props)

	if err != nil {
		return err
	}

	return nil
}

// Send a message as the specified user to the specified channel
func (p *Plugin) sendUserMessage(userID string, channelID string, message string, props model.StringInterface) (*model.Post, error) {
	newPost := &model.Post{
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
	}

	props[DoorLockBotMessageMarker] = true

	newPost.SetProps(props)

	err := p.client.Post.CreatePost(newPost)
	if err != nil {
		p.client.Log.Error("error sending message", "error", err)
		return nil, err
	}

	return newPost, nil
}

func (p *Plugin) sendBotMessage(channelID string, message string, props model.StringInterface) error {
	_, err := p.sendUserMessage(p.botUserID, channelID, message, props)
	return err
}

var DoorLockBotMessageMarker = "doorLockBotMessage"
