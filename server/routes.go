package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/mattermost/mattermost/server/public/model"
)

func (p *Plugin) integrationResponse(w http.ResponseWriter, r *http.Request) {
	data := new(model.PostActionIntegrationRequest)
	if err := json.NewDecoder(r.Body).Decode(data); err != nil {
		p.client.Log.Error("error data decoding", "error", err)
		return
	}

	// Check if user is member of p.doorLockChannel
	members, err := p.client.Channel.ListMembersByIDs(p.doorLockChannel, []string{data.UserId})
	if err != nil {
		p.client.Log.Error("error getting channel member", "error", err)
		return
	}

	if len(members) == 0 {
		// User is not a member of the channel
		if err := p.sendMembershipMessage(data.ChannelId); err != nil {
			p.client.Log.Error("error sending membership message", "error", err)
			return
		}

		return
	}

	action := data.Context["response"].(string)
	switch action {
	case "lock":
		if err := p.nukiClient.Lock(r.Context()); err != nil {
			p.client.Log.Error("error nuki lock", "error", err)
			return
		}
	case "open":
		if err := p.nukiClient.Unlock(r.Context()); err != nil {
			p.client.Log.Error("error nuki unlock", "error", err)
			return
		}
	}

	activity := &Activity{
		Owner:  data.UserId,
		Action: action,
	}

	if err := p.notifyNewActivity(activity); err != nil {
		p.client.Log.Error("error notifying new activity", "error", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (p *Plugin) handleNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "Not found: %s %s", r.Method, r.URL.Path)
}

type APIError struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}

func (p *Plugin) writeError(w http.ResponseWriter, err error, message string, code int) {
	// write error
	w.WriteHeader(code)
	resp := APIError{
		Error:   err.Error(),
		Message: message,
	}

	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		p.client.Log.Error("error writing error response", "error", err)
	}
}
