# Mattermost Door Lock plugin

## Installation

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Run `make dist` to build the plugin. This will generate a .tar.gz file.
4. Log in to your Mattermost server.
5. Go to **System Console > Plugin Management**.
6. Ensure that your Mattermost configuration allows plugin uploads.
7. Click **Upload Plugin** and select the .tar.gz file you generated earlier.
8. After the upload completes, click **Activate** to start using the Reservation plugin.

## Configuration
1. Set the Door Lock channel ID
    - only members of this channel can open and lock the door.
2. Set the Nuki API key
    - Nuki API key can be retrieved from: https://web.nuki.io/#/pages/web-api
3. Set the Smartlock ID of the lock that you want to control through this bot
    - Can be retrieved from the Nuki WEB ui

## Usage
1. Upon joining the Door Lock channel it will send a message containing a button to lock or unlock the door.


